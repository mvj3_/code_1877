//题目分析:
//看到题目我就想，什么时候停止跳出结束？哪个位置开始比较好？哪个位置是下界？
//这三个问题。想了一会，觉得想不出来，于是用matlab画了一下散点图，
//就是y=abc+a+b+c这个图像特点，特点很明显，每10个数为一个区间，
//在这个区间内这些散点是单调递增的，下一个区间又是重新开始单调递增，
//但是新区间的前几个散点肯定比上一个区间的最后一个点小，
//所以会存在最小的符合题意的。虽然规律很明显，但是也无法确定上界和下界。
//但目测图像可以发现，在40以下范围内附近肯定可以找到最小符合的那个。即从给定的那个数开始，
//往下算40个，就能找到。事实发现是50内才找到。

//题目网址:http://soj.me/1325

#include <iostream>

using namespace std;

int calculateN(int m)
{
    int outPut = m;
    while(true) {
        if (m/10 == 0) {
            outPut += m % 10;
            break;
        } else {
            outPut += m % 10;
            m /= 10;
        }
    }
    return outPut;
}
int main()
{
    int testCase;
    int M;
    int N = 0, n;
    cin >> testCase;
    while(testCase--) {
        cin >> M;
        for (int i = M; i >= M - 45; i--) {
            n = calculateN(i);
            if (n == M)
                N = i;
        }
        cout << N << endl;
        N = 0;
    }
    return 0;
}